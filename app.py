from time import sleep
import imaplib
import email
from operator import itemgetter

from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from flask import Flask, render_template, request
from flask_table import Table, Col

import secrets

app = Flask(__name__)


def get_email():
    mail = imaplib.IMAP4_SSL('imap.gmail.com')
    mail.login(secrets.mail_un, secrets.mail_pw)
    mail.select('inbox')
    _, all_data = mail.search(None, 'ALL')
    mail_ids = all_data[0]
    latest_email_id = int(mail_ids.split()[-1])
    _, data = mail.fetch(str.encode(str(latest_email_id)), '(RFC822)')
    raw_email = data[0][1]
    msg = email.message_from_string(raw_email.decode('utf-8'))
    return msg


def get_token_from_email(msg):
    sender = msg['From']
    if 'info@andanet.com' not in sender:
        raise ValueError('The most recent email is from {}, not Anda.'.format(sender))
    body = msg.get_payload()
    soup = BeautifulSoup(body, 'html.parser')
    token = soup.find(class_='token').getText()
    return token


def login(driver):
    url = 'https://new.andanet.com'
    driver.get(url)
    un_input = driver.find_element_by_name('username')
    un_input.send_keys(secrets.andanet_un)
    pw_input = driver.find_element_by_name('password')
    pw_input.send_keys(secrets.andanet_pw)
    sign_in_button = driver.find_element_by_class_name('sign-in')
    sign_in_button.click()
    print('Logged in')
    sleep(5)


def verify_device(driver):
    """Not always necessary. Selenium will persist the credentials (cookies+)
    necessary for andanet to trust the device. login() must be called first.
    You must also change the email address on the account (manually on andanet.com)
    to southbaysupercontest.
    """
    request_code_button = driver.find_element_by_xpath("//button[contains(text(), 'Request Code')]")
    request_code_button.click()
    print('Requested verification code')
    sleep(10)
    last_email_msg = get_email()
    verification_code = get_token_from_email(last_email_msg)
    print('Found verification code: {}'.format(verification_code))
    code_input = driver.find_element_by_name('token')
    code_input.send_keys(verification_code)
    verify_code_button = driver.find_element_by_xpath("//button[contains(text(), 'Verify Code')]")
    driver.execute_script('arguments[0].click();', verify_code_button)
    print('Submitted verification code')
    sleep(5)


def query_drug(driver, name):
    """Requires a logged-in session, obviously.
    """
    driver.get('https://new.andanet.com/search?q=' + name)
    return driver.page_source


def parse_drug_results(page):
    soup = BeautifulSoup(page, 'html.parser')
    table = soup.find('tbody')
    results = []
    for row in table.find_all('tr'):
        result = {}
        result['vendor'] = 'ANDA'
        result['name'] = row.find(class_='item-name').getText()
        result['manufacturer'] = row.find(class_='manufacturer').find(class_='dd').getText()
        result['pack_size'] = row.find(class_='pack-size').getText()
        result['unit_price'] = row.find(class_='unit-price').getText()
        if result['unit_price']:
            results.append(result)
    sorted_results = sorted(results, key=itemgetter('unit_price'))
    return sorted_results


class ItemTable(Table):
    vendor = Col('Vendor')
    name = Col('Name')
    manufacturer = Col('Manufacturer')
    pack_size = Col('Pack Size')
    unit_price = Col('Unit Price')


@app.route('/')
def home():
    drug = request.args.get('q')
    if drug:
        page = query_drug(driver, drug)
        results = parse_drug_results(page)
    else:
        results = []
    table = ItemTable(results)
    return render_template('layout.html', table=table, drug=drug)


if __name__ == '__main__':
    # The state of the session is persisted across the driver object.
    chrome_options = Options()
    chrome_options.add_argument('--headless')
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--disable-dev-shm-usage')
    driver = webdriver.Chrome(options=chrome_options)
    driver.implicitly_wait(30)
    try:
        login(driver)
        # verify_device(driver)
        app.run(debug=True)
    finally:
        driver.quit()
